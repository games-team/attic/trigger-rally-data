**************
* Installing *
**************

Just unpack "alps.zip" into your Trigger Rally "events" folder.

(You need to have write access to the trigger rally folder. You might do it as root user.)


*************************
* Licencing information *
*************************

The "Alps' Trophy" event was created as free addition to "Trigger Rally" on April 19 2013. (The maps "Maze" and "Dawn" where added on April 28th.)

All included work can be used under the conditions of the GPL 2 (or any later version).

Feel free to adapt how ever you want it. If you want to tell me how I could improve this event, please send me a message: onsemeliot@riseup.net


Have fun!
